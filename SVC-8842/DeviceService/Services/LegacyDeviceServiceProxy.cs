﻿using Fifthplay.BL.UnifiedDeviceModel.Host.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DeviceService.Services
{
    public class LegacyDeviceServiceProxy : ILegacyDeviceServiceProxy
    {
        // to be taken into account or checked:
        // url's will differ from environment where do we store them? 
        
        public void Remove(Guid deviceUuid)
        {
            var factory = CreateFactory();
            var client = factory.CreateChannel();

            client.Remove(deviceUuid);

            factory.Close();
        }

        public ChannelFactory<IDeviceServiceExtension> CreateFactory()
        {
            var binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;
            binding.Security.Message.ClientCredentialType = MessageCredentialType.None;
            var endpoint = new EndpointAddress("net.tcp://fp-dev-app-01.fifthplayadc.lan:865/DeviceServiceExtensionService");
            return new ChannelFactory<IDeviceServiceExtension>(binding, endpoint);
        }
    }
}
