﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace Fifthplay.BL.UnifiedDeviceModel.Host.Services
{
    // in this sample the contract is not shared but duplicated.
    [ServiceContract]
    public interface IDeviceServiceExtension
    {
        [OperationContract]
        void Remove(Guid deviceUuid);
    }
}
