﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeviceService.Services
{
    public interface ILegacyDeviceServiceProxy
    {
        void Remove(Guid deviceUuid);
    }
}
