﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeviceService.Services;
using Microsoft.AspNetCore.Mvc;

namespace DeviceService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private ILegacyDeviceServiceProxy _legacyDeviceServiceProxy;
        public DeviceController(ILegacyDeviceServiceProxy legacyDeviceServiceProxy)
        {
            _legacyDeviceServiceProxy = legacyDeviceServiceProxy;
        }
        // DELETE api/devices/
        [HttpDelete("{deviceGuid}")]
        public void Delete(Guid deviceGuid)
        {

            // todo check async / await together with begin and end. 

            _legacyDeviceServiceProxy.Remove(deviceGuid);
        }
    }
}
