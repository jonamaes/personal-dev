DECLARe @loginName varchar(30)
DECLARE @customerId Int 

SET @loginName = 'testset33%'

-- get customer
select @customerId = [CustomerId]
from TblCustomer
where LoginName like @loginName

-- get all devices of the fifthgate
select *
from cm_lod_devices d
inner join Tblfifthgate g
on g.fifthgateID = dev_fifthgates_id
where g.CustomerID = @customerId