Get-MsmqQueue -name "*fifthplay*" -QueueType Private  | 
Where-Object MessageCount -gt 0 |  
Sort-Object -Property MessageCount -Descending | 
Select -Property MessageCount, QueueName
